package data_subscription_service;

import data_subscription_service.events.ISubscriptionEventType;

public interface IDataSubscriptionSystem<T>
{
    Class<T> getGenericType();

    boolean addSubscriber(ISubscriber _subscriber);

    boolean removeSubscriber(ISubscriber _subscriber);

    void post(T _data, ISubscriptionEventType _eventType);
}
