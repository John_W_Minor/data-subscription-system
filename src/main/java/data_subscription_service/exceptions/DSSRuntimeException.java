package data_subscription_service.exceptions;

import data_subscription_service.DSSLogging;
import logging.IAmpexLogger;

public class DSSRuntimeException extends RuntimeException
{
    public DSSRuntimeException()
    {

    }

    public DSSRuntimeException(String _message)
    {
        super(_message);
    }

    @Override
    public void printStackTrace()
    {
        IAmpexLogger logger = DSSLogging.getLogger();
        if (logger != null)
        {
            logger.error("", this);
        }
    }
}
