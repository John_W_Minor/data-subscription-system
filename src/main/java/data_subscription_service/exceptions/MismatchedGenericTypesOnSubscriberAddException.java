package data_subscription_service.exceptions;

public class MismatchedGenericTypesOnSubscriberAddException extends DSSRuntimeException
{
    public MismatchedGenericTypesOnSubscriberAddException(Class _dssType, Class _handlerType)
    {
        super(generateMessage(_dssType, _handlerType));
    }

    private static String generateMessage(Class _dssType, Class _handlerType)
    {
        return "\n" +
                "The IDataSubscriptionSystem object's generic type was " + _dssType.getName() + ".\n" +
                "The ISubscriptionEventHandler object's generic type was " + _handlerType.getName() + ".";
    }
}
