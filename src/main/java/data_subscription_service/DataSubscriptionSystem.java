package data_subscription_service;

import data_subscription_service.events.ISubscriptionEventHandler;
import data_subscription_service.events.ISubscriptionEventType;
import data_subscription_service.exceptions.MismatchedGenericTypesOnSubscriberAddException;

import java.util.ArrayList;
import java.util.List;

public class DataSubscriptionSystem<T> implements IDataSubscriptionSystem<T>
{
    private final Class<T> genericType;

    private List<ISubscriber> subscribers = new ArrayList<>();

    public DataSubscriptionSystem(Class<T> _genericType)
    {
        genericType = _genericType;
    }

    @Override
    public Class<T> getGenericType()
    {
        return genericType;
    }

    @Override
    public boolean addSubscriber(ISubscriber _subscriber)
    {
        ISubscriptionEventHandler handler = _subscriber.getSubscriptionHandler();

        Class handlerDataType = handler.getGenericType();

        if (genericType != handlerDataType)
        {
            throw new MismatchedGenericTypesOnSubscriberAddException(genericType, handlerDataType);
        }

        if (subscribers.contains(_subscriber))
        {
            return false;
        }

        return subscribers.add(_subscriber);
    }

    @Override
    public boolean removeSubscriber(ISubscriber _subscriber)
    {
        return subscribers.remove(_subscriber);
    }

    @Override
    public void post(T _data, ISubscriptionEventType _eventType) throws RuntimeException
    {
        try
        {
            for (ISubscriber subscriber : subscribers)
            {
                //This actually is checked, just not in this method.
                //It's checked in the addSubscriber() method.
                subscriber.getSubscriptionHandler().handle(_data, _eventType);
            }
        } catch (RuntimeException e)
        {
            throw e;
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
