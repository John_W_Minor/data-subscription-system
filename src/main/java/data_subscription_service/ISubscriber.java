package data_subscription_service;

import data_subscription_service.events.ISubscriptionEventHandler;

public interface ISubscriber
{
    ISubscriptionEventHandler getSubscriptionHandler();
}
