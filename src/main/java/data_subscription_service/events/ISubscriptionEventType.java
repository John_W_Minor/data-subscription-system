package data_subscription_service.events;

public interface ISubscriptionEventType
{
    String name();
}
