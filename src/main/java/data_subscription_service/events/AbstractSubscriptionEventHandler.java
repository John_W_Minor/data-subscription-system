package data_subscription_service.events;

import data_subscription_service.ISubscriber;

public abstract class AbstractSubscriptionEventHandler<T, O extends ISubscriber> implements ISubscriptionEventHandler<T, O>
{
    private final Class<T> genericType;

    protected AbstractSubscriptionEventHandler(Class<T> _genericType)
    {
        genericType = _genericType;
    }

    @Override
    public Class<T> getGenericType()
    {
        return genericType;
    }
}
