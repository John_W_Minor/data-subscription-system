package data_subscription_service.events;

import data_subscription_service.ISubscriber;

public interface ISubscriptionEventHandler<T, O extends ISubscriber>
{
    Class<T> getGenericType();

    void setOwner(O _owner);

    void handle(T _data, ISubscriptionEventType _eventType) throws Exception;
}
