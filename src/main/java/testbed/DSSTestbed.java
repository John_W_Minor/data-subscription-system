package testbed;

import data_subscription_service.DSSLogging;
import data_subscription_service.DataSubscriptionSystem;
import data_subscription_service.ISubscriber;
import data_subscription_service.IDataSubscriptionSystem;
import logging.IAmpexLogger;

import java.util.List;

/**
 * Data Subscription System library created by John Minor
 */
public class DSSTestbed
{
    public static void main(String[] args) throws Exception
    {
        DSSLogging.startLogging();

        ISubscriber testSubscriber = new TestSubscriber();

        IDataSubscriptionSystem<IAmpexLogger> loggerDSS = new DataSubscriptionSystem<>(IAmpexLogger.class);


        System.out.println();
        System.out.println("With no subscribers and logging enabled:");

        loggerDSS.post(DSSLogging.getLogger(), TestEventTypesEnum.DO_LOG);
        loggerDSS.post(DSSLogging.getLogger(), TestEventTypesEnum.DO_NOT_LOG);


        System.out.println();
        System.out.println("With a subscriber and logging enabled:");

        loggerDSS.addSubscriber(testSubscriber);

        loggerDSS.post(DSSLogging.getLogger(), TestEventTypesEnum.DO_LOG);
        loggerDSS.post(DSSLogging.getLogger(), TestEventTypesEnum.DO_NOT_LOG);


        System.out.println();
        DSSLogging.stopLogging();


        System.out.println();
        System.out.println("With a subscriber and logging disabled:");

        loggerDSS.post(DSSLogging.getLogger(), TestEventTypesEnum.DO_LOG);
        loggerDSS.post(DSSLogging.getLogger(), TestEventTypesEnum.DO_NOT_LOG);


        System.out.println();
        System.out.println("With a generic type mismatch on the DSS and the Subscriber's event handler:");

        Thread.sleep(1000);

        IDataSubscriptionSystem<List> listDSS = new DataSubscriptionSystem<>(List.class);

        listDSS.addSubscriber(testSubscriber);
    }
}
