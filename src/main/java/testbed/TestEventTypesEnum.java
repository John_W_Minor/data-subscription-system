package testbed;

import data_subscription_service.events.ISubscriptionEventType;

public enum TestEventTypesEnum implements ISubscriptionEventType
{
    DO_LOG,
    DO_NOT_LOG,
}
