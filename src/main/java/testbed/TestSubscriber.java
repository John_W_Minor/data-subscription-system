package testbed;

import data_subscription_service.ISubscriber;
import data_subscription_service.events.AbstractSubscriptionEventHandler;
import data_subscription_service.events.ISubscriptionEventHandler;
import data_subscription_service.events.ISubscriptionEventType;
import logging.IAmpexLogger;

public class TestSubscriber implements ISubscriber
{
    private ISubscriptionEventHandler<IAmpexLogger, TestSubscriber> handler;

    public void doThing(IAmpexLogger _logger)
    {
        if (_logger != null)
        {
            _logger.info("hooray! we have logger!");
        } else
        {
            System.out.println("hooray! we don't have a logger!");
        }
    }

    @Override
    public ISubscriptionEventHandler<IAmpexLogger, TestSubscriber> getSubscriptionHandler()
    {
        if (handler == null)
        {
            handler = new AbstractSubscriptionEventHandler<IAmpexLogger, TestSubscriber>(IAmpexLogger.class)
            {
                private TestSubscriber testSubscriber;

                @Override
                public void setOwner(TestSubscriber _owner)
                {
                    testSubscriber=_owner;
                }

                @Override
                public void handle(IAmpexLogger _data, ISubscriptionEventType _eventType) throws Exception
                {
                    if (_eventType == TestEventTypesEnum.DO_LOG)
                    {
                        testSubscriber.doThing(_data);
                    }
                }
            };

            handler.setOwner(this);
        }

        return handler;
    }
}
